from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import DeleteView, FormView, ListView, UpdateView
from rest_framework.generics import CreateAPIView
from rest_framework.renderers import JSONRenderer
from .scoring_integrations.moni import MoniScorer
from .forms import CreditRequestForm
from .models import CreditRequest
from .serializers import CreditRequestCreateSerializer


class CreditRequestFormView(FormView):
    """
    View to render the form used to send a credit request
    """
    form_class = CreditRequestForm
    template_name = 'credit_requests/credit_request_form.html'


class CreditRequestCreateAPIView(CreateAPIView):
    """
    API View to create the credit request and return the result
    """
    renderer_classes = (JSONRenderer,)
    serializer_class = CreditRequestCreateSerializer

    def perform_create(self, serializer):
        # Saving the CreditRequest
        credit_request = serializer.save()
        # Getting the score
        scorer = MoniScorer(
            document_number=credit_request.document_number,
            gender=credit_request.gender,
            email=credit_request.email
        )
        credit_request.result = scorer.get_score()
        credit_request.save()


class CreditRequestListView(LoginRequiredMixin, ListView):
    """
    View to list credit requests
    """
    model = CreditRequest
    template_name = 'credit_requests/credit_request_list.html'


class CreditRequestUpdateView(LoginRequiredMixin, UpdateView):
    """
    View to update a credit
    """
    model = CreditRequest
    fields = [
        'document_number', 'name', 'surname', 'gender', 'email', 'ammount',
        'result'
    ]
    template_name = 'credit_requests/credit_request_update.html'
    success_url = reverse_lazy('credit_request_list_view')


class CreditRequestDeleteView(LoginRequiredMixin, DeleteView):
    model = CreditRequest
    template_name = 'credit_requests/credit_request_delete.html'
    success_url = reverse_lazy('credit_request_list_view')

$(document).ready(function() {
    $('select').material_select();
    $('#modal_result').modal();
    $('#modal_loader').modal({dismissible: false});
});

function action_submit(e) {
    e.preventDefault();
    clean_errors();
     $('#modal_loader').modal('open');

    data = {
    	"document_number": $( "#id_document_number" ).val(),
    	"name": $( "#id_name" ).val(),
    	"surname": $( "#id_surname" ).val(),
    	"email": $("#id_email").val(),
    	"gender": $("#id_gender").val(),
    	"ammount": $("#id_ammount").val()
    }
    var target_url = $(e.currentTarget).attr("target_url");

    $.ajax({
        url: target_url,
        method: "POST",
        data: data,
        beforeSend: function beforeSend(xhr) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get('csrftoken'));
        },
        success: function success(data) {
        	$('#modal_loader').modal('close');
        	$('#modal_result_text').text(data.result_display);
        	$('#modal_result').modal('open');
        },
        error: function error(xhr, ajaxOptions, thrownError) {
        	$('#modal_loader').modal('close');
            $.each(xhr.responseJSON, function(i, field) {
            	add_error(i, field[0])
            });
        }
    });
}

function add_error(id_field, message) {
    var error = $('<a />').attr(
        {
            "href": id_field,
            "style": "color: red;"
        }).html('<i class="material-icons">warning</i> ' + message);
    $('#' + id_field + '_error').append(error);
}

function clean_errors() {
	$(".validation_errors").empty();
}


// Events Binds
$(document).on('click', '#action_submit', action_submit);

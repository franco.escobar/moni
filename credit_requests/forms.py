from django.forms import ModelForm
from .models import CreditRequest


class CreditRequestForm(ModelForm):
    """
    Form to create a credit request
    """
    class Meta:
        model = CreditRequest
        fields = [
            'document_number', 'name', 'surname', 'gender', 'email', 'ammount'
        ]

from django.test import TestCase
from .models import CreditRequest
from .scoring_integrations.moni import MoniScorer


class MoniScorerTest(TestCase):

    CORRECT_DATA = {
        'document_number': 30000000,
        'gender': 'M',
        'email': 'test@test.com'
    }

    def test_correct_data(self):
        scorer = MoniScorer(**self.CORRECT_DATA)
        self.assertEqual(scorer.get_score(), CreditRequest.APPROVED)

    def test_wrong_document_number(self):
        data = self.CORRECT_DATA.copy()
        data.update({'document_number': -1})
        scorer = MoniScorer(**data)
        self.assertEqual(scorer.get_score(), CreditRequest.DATA_ERROR)

    def test_wrong_gender(self):
        data = self.CORRECT_DATA.copy()
        data.update({'gender': 'X'})
        scorer = MoniScorer(**data)
        self.assertEqual(scorer.get_score(), CreditRequest.DATA_ERROR)

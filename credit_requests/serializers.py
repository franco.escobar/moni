from rest_framework import serializers

from .models import CreditRequest


class CreditRequestCreateSerializer(serializers.ModelSerializer):
    result_display = serializers.CharField(
        source='get_result_display',
        read_only=True
    )

    class Meta:
        model = CreditRequest
        fields = [
            'document_number', 'name', 'surname', 'gender', 'email', 'ammount',
            'result_display'
        ]

import requests
from credit_requests.models import CreditRequest


class MoniScorer(object):
    """
    Class for getting information of scoring from Moni service
    """
    def __init__(self, document_number, gender, email):
        self.document_number = document_number
        self.gender = gender
        self.email = email

    def _get_url(self):
        """
        Returns the service's url
        """
        return 'http://scoringservice.moni.com.ar:7001/api/v1/scoring/'

    def _get_params(self):
        """
        Returns the request params
        """
        return {
            'document_number': self.document_number,
            'gender': self.gender,
            'email': self.email
        }

    def _get_score_from_response(self, response):
        """
        Given the response, returns the score result
        """
        json_data = response.json()
        if json_data.get('error') is True:
            result = CreditRequest.DATA_ERROR
        elif json_data.get('approved') is True:
            result = CreditRequest.APPROVED
        else:
            result = CreditRequest.DISAPPROVED
        return result

    def get_score(self):
        """
        Returns the score for the current datas
        """
        try:
            response = requests.get(self._get_url(), self._get_params())
            assert(response.status_code == requests.status_codes.codes.OK)
        except (requests.exceptions.RequestException, AssertionError):
            result = CreditRequest.REQUEST_ERROR
        else:
            result = self._get_score_from_response(response=response)
        return result

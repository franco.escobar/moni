from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse_lazy
from .models import CreditRequest


class CreditRequestCreateAPIViewTest(APITestCase):
    """
    Tests the Credit Request creation
    """
    DATA = {
        'document_number': 30000000,
        'name': 'Test',
        'surname': 'Test',
        'gender': 'M',
        'email': 'hola@mail.com',
        'ammount': 10
    }

    def setUp(self):
        super(CreditRequestCreateAPIViewTest, self).setUp()
        self.url = reverse_lazy('credit_request_create_api_view')

    def test_post(self):
        self.client.post(self.url, self.DATA)
        self.assertEqual(
            CreditRequest.objects.count(),
            1
        )
        self.assertEqual(
            CreditRequest.objects.last().result,
            CreditRequest.APPROVED
        )

from decimal import Decimal
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class CreditRequest(models.Model):
    """
    A class for storing Credit Requests of users
    """
    MIN_DOCUMENT_NUMBER = Decimal(1000000)
    MALE, FEMALE = 'M', 'F'
    GENDER_CHOICES = (
        (MALE, _("Masculino")),
        (FEMALE, _("Femenino")),
    )
    MIN_AMMOUNT = Decimal(0)
    PENDING, APPROVED, DISAPPROVED, DATA_ERROR, REQUEST_ERROR = list(range(5))
    RESULT_CHOICES = (
        (PENDING, _("Solicitud pendiente")),
        (APPROVED, _("Solicitud Aprobada")),
        (DISAPPROVED, _("Solucitud Desaprobada")),
        (DATA_ERROR, _("Los datos de la solicitud son incorrectos")),
        (REQUEST_ERROR, _("Hubo un error al analizar la solicitud"))
    )

    document_number = models.DecimalField(
        max_digits=8,
        decimal_places=0,
        validators=[MinValueValidator(MIN_DOCUMENT_NUMBER)],
        verbose_name="DNI"
    )
    name = models.CharField(max_length=256, verbose_name="Nombre")
    surname = models.CharField(max_length=256, verbose_name="Apellido")
    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES,
        default=MALE,
        verbose_name="Genero"
    )
    email = models.EmailField()
    ammount = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        validators=[MinValueValidator(MIN_AMMOUNT)],
        verbose_name="Monto"
    )
    result = models.PositiveSmallIntegerField(
        choices=RESULT_CHOICES,
        default=PENDING,
        verbose_name="Resultado"
    )

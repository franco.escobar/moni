from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from .views import (
    CreditRequestCreateAPIView,
    CreditRequestDeleteView,
    CreditRequestFormView,
    CreditRequestListView,
    CreditRequestUpdateView
)

urlpatterns = [
    # Non authenticated users urls
    url(
        r'^$',
        CreditRequestFormView.as_view(),
        name='credit_request_form_view'
    ),
    # Login and Logout urls
    url(r'^accounts/', include([
        url(
            r'^login/$',
            auth_views.login,
            {'template_name': 'credit_requests/login.html'},
            name='login'
        ),
        url(
            r'^logout/$',
            auth_views.logout,
            {'next_page': 'login'},
            name='logout'
        ),
    ])),
    # Administration ruls
    url(r'^administration/', include([
        url(r'^credit_request/', include([
            url(
                r'^$',
                CreditRequestListView.as_view(),
                name='credit_request_list_view'
            ),
            url(r'^(?P<pk>\d+)/', include([
                url(
                    r'^update/$',
                    CreditRequestUpdateView.as_view(),
                    name='credit_request_update_view'
                ),
                url(
                    r'^delete/$',
                    CreditRequestDeleteView.as_view(),
                    name='credit_request_delete_view'
                ),
            ])),

        ])),
    ])),
    # API urls
    url(r'^api/', include([
        url(r'^credit_requests/', include([
            url(r'^create/$',
                CreditRequestCreateAPIView.as_view(),
                name='credit_request_create_api_view'),
        ])),
    ])),
]

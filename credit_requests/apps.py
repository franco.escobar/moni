from django.apps import AppConfig


class CreditRequestsConfig(AppConfig):
    name = 'credit_requests'
